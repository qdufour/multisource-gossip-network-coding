\begin{figure}[h]
  \centering
  \includegraphics{img/stack.png}
  \caption{Stack\label{fig:stack}}
\end{figure}

Improve information spreading without sending more messages, reduce message redundancy, improving probability of sending new information, must encode between them messages sent in the same "timeframe", using Lamport Timestamp to do so, as messages with same generation are messages sent during the dissemination of each other. Provided as a middleware between the application and the network as pictured in figure~\ref{fig:stack}.

\subsection{RLNC with unordered events}

Unfortunately, the source doesn't know what is the position of its message in the encoding vector, as the source doesn't know the other messages, neither their number. But the only thing we need is that every coefficients for a given message have the same position in the encoding vector. As we are unable to give it a position at sending, we generate a unique id for the message and associate it with the coefficient value. When sending an encoded packet, instead of sending a vector $c^1,...,c^n$, we send $m$ tuples, corresponding to the known messages, containing the message id and their coefficient: $(id(M^{i_1}), c^{i_1}),...,(id(M^{i_m}), c^{i_m})$.

We create the encoding vector at the message reception. Each node maintains an ordered list of known messages ids (which is different from nodes to nodes), corresponding to the order of the messages in the coefficient vectors used in the matrix.
If a new message id is received, its id is appended to the list of known messages ids.
In every cases, the message id position becomes the coefficient position in the encoding vector.
After that, this new encoding vector and its associated data will be appended to the decoding matrix.

The inverse operation is realized when a message is about to be sent. The generated encoding vector is converted to a tuple list containing the message id and its value.

To work, RLNC needs same length messages.
However, we don't need to proactively pad short messages, at least we can set a maximum size after which messages are cut in two or more messages.

If the message length is stored inside the message or the message is terminated by a distinctive symbol (like 0), we can pad encoded packets allowing to lazily pad messages. If two messages have different size, the shortest one is padded with its last byte. We can't pad with random values or fixed values, as these values will prevent the decoding of the message. This technique allows us to save bytes when all messages are short, preventing us from padding every messages to a standard
size for nothing.

%\textbf{Encode messages that are currently disseminated}

\subsection{The protocol}

We now present a detailed description of our modified version of Infect and Die for RLNC. The pseudo-code is given in Algorithm~\ref{algo1}.
Each node is configured with a fanout $k$, and two values to configure the size of the generation size: $switch$ and $split$.
Besides the configuration, each node maintains a set of $rcv$ received but not decoded packets, $dlv$ a set of decoded and delivred packets and a value $g$, similar to a Lamport timestamp, which will be used by the node when it will need to broadcast new events to set their generation.

This protocol has no background thread as it is reactive, and is executed only when a packet is received from another node via \textsc{Receive} or the local application wants to disseminate an event with \textsc{Broadcast}.

When \textsc{Broadcast} is called, the application provides a parameter $e$ containing the content of the event, which is considered as a list of bytes at this layer.
A unique identifier is assigned to the event via \textsc{UniqueID}, which can be a random identifier with low probability of collision. This case is discussed in section~\ref{id-for-evts}.
To have an encodable packet, we also need a generation which is also used at the same time by other processes but not too many, this ideal value is stored inside the $g$ variable.
Once created, the encodable packet is not directly sent.
Instead, it is added to the local set of received packets and delivered packets.
After that, new packets created from the generation of our event are sent on the network via \textsc{Disseminate} and $g$ is updated if needed via \textsc{NextGen}.

When \textsc{Receive} is called, a packet must be provided as parameter. First, the packet is added to the set of received packet and \textsc{Decode} is called for the packet generation, which performs a Gauss-Jordan elimination on this generation.
If new information has been added in the generation by the packet, the rank of the generation has increased.
If no new information has been received, we do nothing.
Otherwise, we search in the current generation if some events have been totally decoded with the help of this new packet, if so the event is added to the set of delivered packets and delivered to the application.
Next, we disseminate the generation of the packet (and not the generation $g$) with \textsc{Disseminate} and update $g$ if needed via \textsc{NextGen}.

\textsc{Disseminate} is a simple helper to send encoded packets to other random nodes of the network.
It generates $k$ recursively encoded packets (with \textsc{Recode}) from the desired generation that are sent to the network layer \textsc{Send}. Peer selection is done in the network layer.

\textsc{NextGen} is another helper to increase the encoding generation $g$, which is used as a Lamport clock.
The value of the clock in incremented if the received value is bigger than the current value or if a local event occured.
The received value corresponds to the generation of a received packet and the local events to the rank of the generation pointed by $g$ being bigger than a given threshold $switch$.
Increment is not necessarly of one, in our case we chose to use the
$split$ configuration parameter.
The impact of $switch$ and $split$ is discussed in section~\ref{events-to-gen}.

\input{fragments/algo-push.tex}

\subsection{Controlling generations size}
\label{events-to-gen}

Generation size is linked to the dissemination time of a message. Indeed, if we don't use $split$ and $switch$, messages of same generation will be messages sent by nodes that didn't receive yet a message sent of the same generation by another node. Dissemination time, in turn, is influenced by the events emission speed, the size of the network, the fanout and the latency.
However, we are interested by controlling in a certain extent the size of these generations.
Indeed, we have to find a tradeoff between small generations and big generations, as bigger a generation is, lesser messages are needed to disseminate events but harder it is to decode an event and bigger each sent messages are (due to bigger headers).

We introduced two values, named $switch$ and $split$, to counterbalance the effects of the environment. $switch$ requires to receive a certain number of messages before emitting to a new generation, which in turn increase the generation size. Conversely, $split$ allow each nodes to broadcast messages on a generation which is a multiple of $split$ plus a random shift in $[0;shift[$.

\subsection{Creating identifiers for events}
\label{id-for-evts}

As we use random ids to identify messages inside a generation, we are subject to collisions that will prevent the decoding of many messages.
Choosing a random id is analog to the birthday problem as we want only unique ids inside a generation.
In our case, more we use bytes to generate a random id, lesser is the collision probability.
An approximation of collision probability is given by the following formula:

\[ P(collision) = 1 - e^{-n^2 / 2^{8b}} \]

Where $n$ is the number of messages in a generation and $b$ is the number of bytes.
We want to find values where the collision probability is as small as possible without wasting too many bytes:

\[\forall n \leq 100,\quad b=4,\quad P(collision) < 10^{-5} \]

With these values, it enables us to encode coefficients on 5 bytes: 4 bytes for the message id and 1 byte for the value.

\subsection{Headers size limit}
Practically, it appears that the limiting factor with events will be the size of the headers, which is a strong limit.
In the Internet, a typical maximum packet size, excluding bytes needed for underlying protocols, is a bit more than 1400 bytes.
If we set the maximum headers size in the worst case to 10\%, we have 140 bytes left for our headers.

Our packet structure will be like the one presented in figure~\ref{fig:pkt-struct} with $gen$ being the generation identifier, $n$ the number of coefficients, $m_i$ and $v_i$ the message identifier and coefficient identifier of one of the encoded message in the payload. With this structure, we can encode until 27 packets at the same time.

\begin{figure}[h]
\begin{bytefield}[bitwidth=0.06\linewidth]{14}
\bitheader{0-13} \\
\bitbox{2}{$gen$}
\bitbox{2}{$n$}
\bitbox{4}{$m_1$}
\bitbox{1}{$v_1$}
\bitbox{4}{$m_2$}
\bitbox{1}{$v_2$}
\bitbox{1}{...} \\
\bitbox{15}{$payload$}
\end{bytefield}
\centering
\caption{Packet structure\label{fig:pkt-struct}}
\end{figure}
