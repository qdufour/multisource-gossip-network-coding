\section{Background on network coding and challenges}

\begin{figure}[h]
\begin{tikzpicture}

% nodes
\node (A) at (0, 2) {A};
\node (B) at (0, 0) {B};
\node (C) at (2.8, 1) {C};
\node (D) at (8, 2) {D};
\node (E) at (8, 0) {E};

  \path[->, every node/.style={sloped,anchor=south}]
  (A) edge node[style={anchor=north,pos=.3}]{$M^1$} (C) 
  (A) edge node[style={anchor=north}]{$M^1$} (D)
  (B) edge node[style={anchor=south,pos=.3}]{$M^2$} (C) 
  (B) edge node{$M^2$} (E)
  (C) edge node[style={anchor=north,pos=.75}]{$\mathbf{c^1}, c^1_1 M^1 + c^1_2 M^2$} (D)
  (C) edge node[style={anchor=south,pos=.75}]{$\mathbf{c^2}, c^2_1 M^1 + c^2_2 M^2$} (E);

\end{tikzpicture}
  \caption{With RLNC, C can send useful information to D and E without knowing what they have received}
  \label{fig:rlnc}
\end{figure}

RLNC\cite{fragouli_network_2006} provides a way to combine different
messages on a network to improve their dissemination speed by
increasing the chance that receiving nodes learn something new. In
Figure~\ref{fig:rlnc}, node C cannot know what D and E have
received. By sending a linear combination of $M^1$ and $M^2$, nodes D
and E can respectively recover $M^2$ and $M^1$ with the help of the
plain message they also received.  Without RLNC, node C would have to
send both $M^1$ and $M^2$ to D and E involving two more messages.
Every message must have the same size, defined as $L$ bits
thereafter.  To handle messages of different size, it is possible to
split or pad the message to have a final size of $L$ bits.
The message content has to be split as symbols over a field
$\mathbb{F}_{2^n}$.
%The $\mathbb{F}_{2^8}$ field has interesting
%properties when doing RLNC.  First, a byte can be represented as a
%symbol in this field. Thereafter, this field is small enough to speed
%up some computing with discrete logarithm tables and at the same time
%sufficiently large to guarantee linear independence of the random
%coefficients with very high probability.

Encoded messages are linear combinations over $\mathbb{F}_{2^n}$ of
multiple messages. This linear combination is not a concatenation: if
the original messages are of size $L$, the encoded messages will be of
size $L$ too.  An encoded message carries a part of the information of
all the original messages, but not enough to recover any original
message. After receiving enough encoded messages, the original
messages will be decodable.

To perform the encoding, the sources must know $n$ original messages
defined as $M^1,...,M^n$.  Each time a source wants to create an
encoded message, it randomly chooses a sequence of coefficients
$c_1,...,c_n$, and computes the encoded message $X$ as follows: $X =
\sum_{i=1}^{n} c_i M^i$. An encoded message thus consists of a
sequence of coefficients and the encoded information: $(c, X)$. 

Every participating node can recursively encode new messages from the
one they received, including messages that have not been decoded.  A
node that received $(c^1, X^1),...,(c^m, X^m)$ encoded messages, can
encode a new message $(c',X')$ encoded by choosing a random set of
coefficients $d_1,...,d_m$, computing the new encoded information $X'
= \sum_{j=1}^{m} d_j X^j$ and computing the new sequence of
coefficients $c_i' = \sum_{j=1}^{m} d_j c^j_i$.  

An original message $M^i$ can be considered as an encoded message by
creating a coefficient vector $0,...,1,..,0$ where 1 is at the ith
position. The encoding of a message can therefore be considered as a
subset of the recursive encoding technique.

Even if there is no theoretical limit on the number $n$ of messages
that can be encoded together, there are two reasons to limit it.
First, Gauss-Jordan elimination has an $O(n^3)$ complexity, which
becomes rapidly too expensive to compute.  Then, the more the messages
encoded together, the bigger the sequence of coefficients while the
encoded information remains stable. In extreme cases this can result
in sending mainly coefficients on the network instead of information.
To encode more data, splitting messages in groups named generations
solves the previous problems, as only messages in the same generation
are encoded together.
However applying network coding to epidemic dissemination raises
several challenges. 

\paragraph{Assigning a message to a generation} Generations often
consist of integers attached to messages. Messages with the same
generation value are considered in the same generation and can be
encoded together. The value must be assigned in such a way that enough
messages are in the same generation to benefit from RLNC properties
but not too many to keep the decoding complexity sufficiently low and
to limit the size of the coefficients sent on the network. In a
single-source scenario, the size of the generation is a parameter of
the protocol, and is determined by counting the number of messages
sent in a given generation.  However, with multiple sources, there is
no way to know how many messages have been sent in a given generation.

\paragraph{Sending coefficients on the network} Coefficients are
generally sent under the form of a dense vector over the network. Each
value in the vector is linked to a message. % That is to consider that
% messages are named over a namespace that has all its value assigned to
% a message. 
In a single-source scenario, this is not a problem: the source knows
in advance how many messages it will send and can assign each message
a position in the vector and start creating random linear
combinations.  In the case of multiple sources, the number of messages
is not available, and waiting to have enough messages to create a
generation could delay message delivery and above all, the network
traffic required to order the messages in the dense vector would ruin
the benefits of network coding.

\paragraph{Pulling with RLNC} When doing pull-based rumor mongering, a
node must have a way to ask what rumors it needs. Without network
coding, it simply sends a message identifier to ask for a
message. But sending a message identifier in the case of network
coding raises several questions: does the node answer only if it has
decoded the message? Or if it can generate a linear combination
containing this message?

\paragraph{Estimating the number of missing packets} Some gossip
protocols need to estimate the number of missing messages. Without
network coding, the number of missing packets corresponds to the
number of missing messages. But with RLNC, it is possible to have some
linear combinations for a given set of messages but without being able
to decode them. All the messages are considered as missing but one
packet could be enough to decode everything.
