\section{Approach}

\subsection{System model}
Our model consists of a network of $n$ nodes that all run the same
program. These nodes communicate over a unicast unreliable and fully
connected medium, such as UDP over Internet. Nodes can join and leave
at any moment, as no graceful leave is needed, crashes are handled like departures.
We consider that each nodes can obtain the address
of some other nodes of the service via a Random Peer Sampling
service. There is no central authority to coordinates nodes, all
operations are fully decentralized. All messages are exchanged
asynchronously. We consider the exchange of multiple messages and any
node of the network can inject messages in the network, without any
prior coordination between nodes (messages are not pre-labeled).

The \textsc{Broadcast} procedure is used to trigger a message dissemination from a specific node and \textsc{Deliver} is called to notify a node of a message (if the same message is received multiple time, it is delivered only once).
On the other side, a node needs to be able to send messages to random nodes of the network with a RPC-like mechanism. The \textsc{Send} commands enables to call a specific function tagged as \textbf{upon receive} in our pseudo-code on a random remote node.

\subsection{RLNC applied to push pull protocols}

\input{fragments/explaining-algo.tex}

\subsection{Dissemination procedures}

\input{fragments/algo-handler.tex}

We define some primitives in Algorithm \ref{algo3} that aim to be as general as possible to be reusable in other dissemination algorithms (like Infect and Die or Balls and Bins).

In our algorithms, we use a tuple of 3 objects to represent a network coded packet: $\langle g, c, e \rangle$ where $g$ is the generation number, $c$ an ordered set containing the network coding coefficients and finally $e$ is the encoded payload. The coefficient set contains one or more tuple with the following structure: $\langle msg_{id}, coef_{value} \rangle$ where $msg_{id}$ is the message identifier and $coef_{value}$ is the value of the coefficient.

\textsc{NextGen} captures the rule to update the generation.
The passed argument $h$ is the received generation.
As defined by the Lamport timestamps, the clock $g$ is incremented on a local event or on receiving a message by keeping the maximum of both clocks plus one. 
This procedure must be called after any received packet or message injected by the node (the local event).


\textsc{ProcessPacket} aggregates the core tasks to perform on packet
receiving. This procedure returns wether or not the received packet
contained useful information as this information will be used by the
final algorithm. Internally, the node starts by adding the packet to the
matrix and do a Gaussian (line \ref{line:decode}), if decoding the
packet didn't increase the matrix rank, the packet was useless and the processing stops here.
Otherwise, the node must add unknown message identifiers from the packet coefficient list to the known identifiers set.
After that, the node deliver all messages that have been decoded thanks to the received packet.
Finally, the node checks if the clock must be updated.

% Is it really useful to define it here? Could we just name it T_p like in Pulp instead of defining a procedure?
\textsc{GetTradingWindow} returns a subset of the current node's
recent history. This subset is configured by two variables, the trading size wich is the maximum size of the subset and the safety margin which prevent last received values to be put in the trading window as they still might be in the push phase.

\subsection{Push and pull procedures}

\input{fragments/algo-push.tex}

%To have a good dissemination of existing messages, we use a trading window in addition to the coefficient list included in each network coded packets.
%That's why we add to the missing set every message identifiers coming from the trading window and the list of coefficients we don't know yet, for each received push (algorithm \ref{algo1}) and pull (algorithm \ref{algo2}) packets in order to achieve a good dissemination of existing messages.
%Without a trading window, a message which is added lately to a generation will only be forwarded once on the push phase, contrary to previous messages of the generation which have their existence forwarded by all following messages, which is not enough to achieve an atomic broadcast. 

Algorithm~\ref{algo1} corresponds to the push part of our algorithm. 
Each node is configured with a fanout $k$ and a time to live $ttl$.
This part initiates the dissemination of the message, we aims to reach as much nodes as possible without too much redundancy, so we use the following condition: to be forwarded, a packet must be new to the node and its $ttl$ must be superior to zero.

When \textsc{Broadcast} is called, the application provides a parameter $e$ containing the content of the event, which is considered as a list of bytes at this layer.
A unique identifier is assigned to the event via \textsc{UniqueID}, which can be a random identifier with low probability of collision. This case is discussed in section~\ref{id-for-evts}.
To have an encodable packet, we also need a generation which is also used at the same time by other processes but not too many, this ideal value is stored inside the $g$ variable.
Once created, the encodable packet is not directly sent.
Instead, it is added to the local set of received packets and delivered packets.
After that, new packets created from the generation of our event are sent on the network via \textsc{Disseminate} and $g$ is updated if needed via \textsc{NextGen}.

\textsc{Disseminate} is a simple helper to send encoded packets to other random nodes of the network.
It generates $k$ encoded packets (with \textsc{Recode}) from the desired generation that are sent to the network layer via \textsc{Send}. Peer selection is done at the network layer level.

\input{fragments/algo-pull.tex}

For the pull part described on algorithm \ref{algo2}, we reuse the adaptiveness logic of the original Pulp adapted to RLNC.
Indeed, we can't compute the number of missing element with only the missing set.
We must take into account packets that are not totally decoded but not totaly unknown too.
To do that, we want to compute the difference between the known messages identifiers and the number of independant linear combinations we have. We keep messages identifiers in two sets. Before the decoding of the message in $missing$ and after in $dlv$. The number of independant linear combinations is tracked in $rcv$. With these three sets we can compute the number of missing independant linear combinations to decode all known messages (as in line
\ref{line:compute-missing} of algorithm \ref{algo2}).

The only difference with Pulp in the pull thread is that, instead of sending the list of missing message identifiers, we send the list of generations we miss. Because of this choice, we can send way smaller packets during bursts as if we miss 23 messages from the same generation, we send only one integer instead of 23.

When a pull request is received, the node looks if it has some messages for one of the requested generation, if so it sends a linear combination of this generation. On receiving a pull reply, the node simply processes the packet.

\subsection{Coefficients generation and size considerations}
\label{id-for-evts}

As we use random ids to identify messages inside a generation, we are subject to collisions that will prevent the decoding of many messages.
Choosing a random id is analog to the birthday problem as we want only unique ids inside a generation.
In our case, more we use bytes to generate a random id, lesser is the collision probability.
An approximation of collision probability is given by the following formula:

\[ P(collision) \approx 1 - e^{-n^2 / 2^{8b}} \]

Where $n$ is the number of messages in a generation and $b$ is the number of bytes.
We want to find values where the collision probability is as small as possible without wasting too many bytes:

\[\forall n \leq 100,\quad b=4,\quad P(collision) < 10^{-5} \]

With these values, it enables us to encode coefficients on 5 bytes: 4 bytes for the message id and 1 byte for the value.

Practically, it appears that the limiting factor with events will be the size of the headers, which is a strong limit.
In the Internet, a typical maximum packet size, excluding bytes needed for underlying protocols, is a bit more than 1400 bytes.
If we set the maximum headers size in the worst case to 10\%, we have 140 bytes left for our headers.

Our packet structure will be like the one presented in figure~\ref{fig:pkt-struct} with $gen$ being the generation identifier, $n$ the number of coefficients, $m_i$ and $v_i$ the message identifier and coefficient identifier of one of the encoded message in the payload. With this structure, we can encode until 27 packets at the same time.

\begin{figure}[h]
\begin{bytefield}[bitwidth=0.06\linewidth]{14}
\bitheader{0-13} \\
\bitbox{2}{$gen$}
\bitbox{2}{$n$}
\bitbox{4}{$m_1$}
\bitbox{1}{$v_1$}
\bitbox{4}{$m_2$}
\bitbox{1}{$v_2$}
\bitbox{1}{...} \\
\bitbox{15}{$payload$}
\end{bytefield}
\centering
\caption{Packet structure\label{fig:pkt-struct}}
\end{figure}
