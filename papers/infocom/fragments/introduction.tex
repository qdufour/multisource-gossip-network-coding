\section{Introduction}

Distributed systems are becoming increasingly large and complex, with
an ever-growing shift in their scale and dynamics.  This trend leads
to revisit key known challenges such as inconsistencies across
distributed systems due to an inherent increase of unpredictable
failures. To be highly resilient to such failures, an efficient and
robust data-dissemination protocol is the cornerstone of any
distributed systems. In the last decade, gossip protocols, also known
as epidemic protocols, have been widely adopted as a key functional
building block to build distributed systems. For instance, gossip
protocols have been used for overlay construction \cite{tman,
  pleiades}, membership and failure detection
\cite{lakshman_cassandra:_2010, dynamo, lifeguard, Das02swim},
aggregating data \cite{jelasity}, and live streaming \cite{frey},
\cite{frey2}.  This wide adoption comes from the resilience,
simplicity, and natural distribution of gossip protocols
\cite{androulaki_hyperledger_2018}, \cite{lakshman_cassandra:_2010},
\cite{nedelec_crate:_2016}.  Gossip-based dissemination can be simply
represented as the random phone-call problem; at the beginning,
someone learns of a rumor, and calls a set of random friends to
propagate it. As soon as someone learns of a new rumor, in turn, she
randomly propagates it to her own set of friends, and so on
recursively. Further, depending on whether there are one or more
sources of rumors (i.e. dissemination of messages from one or multiple
nodes), gossip protocols may be either single or multi source. In both
cases, randomness and recursive probabilistic exchanges provide
scalability, robustness and fault tolerance under high churn to
disseminate data while staying simple.

However, due to its probabilistic aspect, gossip-based dissemination
implies high redundancy with nodes receiving the same message several
times.  Many algorithms have been studied to limit the number of
exchanged messages to disseminate data, using different combinations of
approaches such as \textit{push} (a node can push a message it knows
to its neighbors), \textit{pull} (a node pulls a messages it does not
know from its neighbors) or \textit{push-pull} (a mix of both) for
either single- or multi-source gossip protocols
\cite{koldehofe_simple_2004}\cite{euster_epidemics_2004}\cite{felber_pulp:_2012}.

In this paper, we make a significant step beyond these protocols, and
provide better performance with respect to the state of the art of
multi-source gossip protocols.

The key principle of our approach is to consider redundancy as a key
advantage rather than as a shortcoming by leveraging Random Linear
Network Coding (RNLC) to provide efficient multi-source gossip-based
dissemination. Indeed, it has been shown that RLNC improves the
theoretical stopping time, i.e., the number of rounds until protocol
completeness, by sending linear combinations of several messages
instead of a given plain message, which increases the probability of
propagating something new to
recipients~\cite{deb_algebraic_2006,haeupler_analyzing_2011}.

Unfortunately, applying RNLC to multi-source gossip protocols is
not without issues, and three key challenges remain open. First,
existing approaches suppose that a vector, where each index identifies
a message with its associated coefficient as a value, is
disseminated. This approach implies a small namespace. 
% We can't say at the same time that the vector will be too big to contain every messages sent in the network AND that we use generations
% So I just removed this part as we didn't invent generations
In the context of multi source, the only option is to choose a random
identifier over a sufficiently large namespace to have a negligible
collision probability. However this does not scale.  Some algorithms
provide a mechanism to rename messages to a smaller
namespace\cite{castaneda_renaming_2011}, but this kind of techniques
are not applicable to gossip protocols as they would substantially
increase the number of exchanged messages, and inherently the delay.
Second, to reduce the complexity of the decoding process, messages are
split in groups named generations. Existing rules to create
generations require having only one sender, which is impractical in
the context of multiple sources. Third, the use of RNLC implies linear
combinations of multiple messages. This leads to potential partial
knowledge of received messages, making precise message pull requests
useless and breaks pull-frequency adjustments based on missing-message
counts.

In this paper, we introduce \approachName, a CHeaper EPidemic
dissemINation approach for multi-source gossip protocols.  To the
best of our knowledge, our approach is the first one to apply RNLC to
multi-source gossip protocols, while overcoming all the inherent
challenges involved by the use of network-coding techniques.

More precisely, we make the following contributions. 
\begin{itemize}
\item We solve the identifier namespace size \textit{via} the use of sparse vectors. Additional headers sent over the network represent 10\% or less of the total message size.
\item We create generations for messages from multiple sources by
  leveraging Lamport timestamps. All messages sharing the same clock
  are in the same generation whatever its source.
\item We overcome the issue of partial message knowledge by providing
  an adaptation of push, pull, push-pull gossip protocols. We pull
  specific generations instead of specific messages.
\item We introduce updated algorithms to make our approach applicable
  to the current state of the art of multi-source gossip protocols.
\item Finally, we evaluate \approachName thoroughly by simulation. We
  show that our solution reduces bandwidth overhead by 25\% and
  delivery delay by 18\% with respect to
  \textsc{Pulp}~\cite{felber_pulp:_2012}, while keeping the same
  properties.
\end{itemize}

%The remainder of the paper is organized as follows: we first ... in Section ...

% As far as we know, no solution to disseminate independant events from
% multiple sources with network coding has been provided yet, as current solutions
% rely on message naming and grouping on a single source node.
% In this paper, we propose a solution that
% harness the benefits of RLNC while allowing the encoding of independant multi sources messages,
% which is inherent to the use cases presented in
% introduction.

% To do so, we keep message identifiers in a sufficiently large namespace to be able to select them randomly.
% Each node transmits the coefficients in a sparse vector, containing only positions for the message identifiers it knows, keeping the data sent over the network reasonable.
% Each sent packet contains a Lamport timestamp, allowing a node to consider all the messages sharing the same clock in the same generation.
% %To work, the same payload should be disseminated to every nodes as once a message will be encoded in the network, it will be impossible to modify it.
% To adapt existing dissemination protocols, we propose to pull specific generations instead of messages and to count missing independant linear combinations to adapt the pull frequency.

% In this article, we propose an algorithm that work for push and pull protocols.
% We show that if our protocol is correctly configured, in certain network conditions, it can reduce the sent data overhead of 28\%, the sent packet overhead of 43\% and the delivery delay of 18\% compared to \textsc{Pulp}, while keeping the same probability of atomic broadcast.



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: ../paper.tex
%%% End: 
