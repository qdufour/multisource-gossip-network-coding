(random-seed 1337)

(define (is-redundant? gf-size received dimension)
  (<
    (random 1.0)
    (exact->inexact (/ 1 (expt gf-size (- dimension received))))))

(define (generate-nodes nodes id max-id)
  (cond
    ((>= id max-id) nodes)
    (#t
      (hashtable-set! nodes id '())
      (generate-nodes nodes (+ id 1) max-id))))

(define (generate-network node-num)
  (generate-nodes (make-eq-hashtable) 0 node-num))


    
