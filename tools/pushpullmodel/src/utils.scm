(library
  (src utils)
  (export
    reduce
    range
    take
    rotate-left
    intersection
    first-common
    first-common-hash)
  (import (chezscheme))

  (define (reduce fn ls acc)
    (cond
      ((null? ls) acc)
      (#t (reduce fn (cdr ls) (fn acc (car ls))))))

  (define (range from to)
    (cond
      ((>= from to) '())
      (#t (cons from (range (+ 1 from) to)))))

  (define (take l c)
    (cond
      ((= c 0) '())
      (#t (cons (car l) (take (cdr l) (- c 1))))))

  (define (rotate-left l)
    (cond
      ((null? l) '())
      (#t
       (append
         (cdr l)
         (cons (car l) '())))))

  (define (intersection l1 l2)
    (cond
      ((null? l1) '())
      ((member (car l1) l2) (cons (car l1) (intersection (cdr l1) l2)))
      (#t (intersection (cdr l1) l2))))

  (define (first-common l1 l2)
    (cond
      ((null? l1) '())
      ((member (car l1) l2) (car l1))
      (#t (first-common (cdr l1) l2))))

  (define (first-common-hash l h)
    (cond
      ((null? l) '())
      ((hashtable-contains? h (car l)) (car l))
      (#t (first-common-hash (cdr l) h))))

)
