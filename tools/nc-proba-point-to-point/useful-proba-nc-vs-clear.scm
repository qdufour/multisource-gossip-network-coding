(define (no-nc dimension already-known)
  (exact->inexact (/ already-known dimension)))

(define (nc gf-size dimension already-known)
  (exact->inexact (/ 1 (expt gf-size (- dimension already-known)))))

(define (show gf-size dimension already-known)
  (cond
    ((= already-known dimension) '())
    (#t
      (format 
        #t
        "~A, ~A, ~A~%"
        already-known
        (no-nc dimension already-known)
        (nc gf-size dimension already-known))
      (show gf-size dimension (+ 1 already-known)))))
      

;(display (nc 256 5 4))
(show 256 20 0)
