pkg load communications

function n = encode_one (coef, msgs)
  n = gf([],8);
  for j = 1:columns(msgs)
    for i = 1:rows(msgs)
      n(j) += msgs(i,j) * coef(i);
    endfor
  endfor
endfunction

function n = encode_with_coef (coefs, msgs)
  n = gf([],8);
  for i = 1:columns(coefs)
    n = [ n ; encode_one(coefs(i,:), msgs) ];
  endfor
endfunction

function [c, e] = encode(msgs)
  c = gf(unidrnd (255, rows(msgs), rows(msgs)), 8);
  e = encode_with_coef(c, msgs);
endfunction

function [c, e] = recode_one (coefs, msgs)
  temp = unidrnd (256, 1, rows(msgs));
  e = encode_one(temp, msgs);
  
  c = gf([], 8);
  for i = 1:columns(coefs)
    for j = 1:rows(coefs)
      c(i) += temp(j) * coefs(j, i);
    endfor
  endfor 
endfunction

function n = decode(coef, enc)
  %{
  Use a Gauss-Jordan elimination instead of a Gauss Elimination
  As Octave doesn't support Gaussian Elimination over Galois Field
  rref fails with error: xnorm: wrong type argument 'galois
  We can't run Gauss-Jordan elimination if we miss some packets unfortunately
  Contrary to Gaussian Elimination
  Which is needed for effective recoding (eliminating non linear independent lines)
  %}
  
  n = coef \ enc;
  % n = rref([coef ; enc]);
endfunction

hello = gf([
  104, 101, 108, 108, 111 ; % packet 1
  119, 111, 114, 108, 100 ; % packet 2
  98, 105, 103, 32, 32      % packet 3
], 8);

%{
[gen_coef, encoded_hello] = encode(hello)
decode(gen_coef, encoded_hello)

[a, b] = recode_one(gen_coef, encoded_hello);
[c, d] = recode_one(gen_coef, encoded_hello);
[e, f] = recode_one(gen_coef, encoded_hello);

decode([a ; c ; e], [ b ; d ; f ])
%}

% node A
[coefa, vala] = deal(gf([1, 0, 0],8), gf([104, 101, 108, 108, 111],8));

% node B
[coefb, valb] = deal(gf([0, 1, 0],8), gf([119, 111, 114, 108, 100],8));

% node C
[coefc, valc] = deal(gf([0, 0, 1],8), gf([98, 105, 103, 32, 32], 8));

% node D
[coefd, vald] = deal([coefa ; coefb ; coefc], [vala ; valb ; valc]);
[c1, v1] = recode_one(coefd, vald);
[c2, v2] = recode_one(coefd, vald);
[c3, v3] = recode_one(coefd, vald);

[c3, v3 ; c1, v1 ; c2, v2]

% node E
decode([c3 ; c1 ; c2], [v3 ; v1 ; v2])
