#!/usr/bin/scheme -q

(define (get-random-nodes start stop)
  (let [(x (- (random 10.0) 5)) (y (- (random 5.0) 2.5))]
    (cond
      ((= start stop) '())
      ((< (sqrt (+ (* x x) (* y y))) 1.5) (get-random-nodes start stop))
      (#t (cons (list start x y) (get-random-nodes (+ start 1) stop))))))

(define (print-node node type)
  (let [(id (car node)) (x (cadr node)) (y (caddr node))]
    (format #t "\\node (~a) at (~a, ~a) {\\~a};~%" id x y type)))

(define (print-nodes nodes type)
  (map (lambda (node) (print-node node type)) nodes))

(define (select-nodes nodes start stop)
  (filter (lambda (node) (and (>= (car node) start) (< (car node) stop))) nodes))

(define-syntax only
  (syntax-rules ()
   [(_ count body ...)
    (begin
      (format #t "\\only<~a> {~%" count)
      body ...
      (format #t "}~%"))]))

(let*
  [(starting-nodes (get-random-nodes 0 100))
   (still-healthy (select-nodes starting-nodes 0 80))
   (broken (select-nodes starting-nodes 80 100))
   (new (get-random-nodes 100 120))] 

  (format #t "\\clip (-5.5, 3) rectangle (5.5, -3.5);")
  (format #t "\\node (origin) at (0, 0) {\\compmailr};~%")

  (print-nodes still-healthy "compmailr")
  (print-nodes broken "compbrok")
  (print-nodes new "compnew")
  (map (lambda (node) (format #t "\\draw[->, black!60!green] (origin.center) edge (~a.center);~%" (car node))) starting-nodes)
  (print-nodes still-healthy "compmailr")
  (print-nodes broken "compbrok")
  (print-nodes new "compnew")

  (format #t "\\node (fail1) at (-2.5, -3.3) {\\includegraphics[scale=0.3]{img/fail.png} BANDWIDTH};")
  (format #t "\\node (fail2) at (0, -3.3) {\\includegraphics[scale=0.3]{img/fail.png} COVERAGE};")
  (format #t "\\node (fail3) at (2, -3.3) {\\includegraphics[scale=0.3]{img/fail.png} DELAY};"))
  (format #t "\\node (origin) at (0, 0) {\\compmailr};~%"))
(exit)
