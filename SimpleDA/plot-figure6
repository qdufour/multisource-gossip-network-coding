#!/usr/bin/bash

if [ "x$1" == "xcompute" ]; then
mkdir -p ./conf_out

# Logical clock with EPTO
cat > ./conf_out/conf.yaml <<EOF
IS_CLOCK_GLOBAL: False              # Use a global or logical clock {parameter}
nbNodes : 100                       # 100 processes {from Figure 6}
probBroadcast : 0.05                # 5% broadcast {from Figure 6}
nbCycles : 100                      # 100 cycles? {Each nodes will broadcast during these number of cycles}
ORDER_MECHANISM: STABLE_ORDER       # EpTO algo (STABLE_ORDER) or regular gossip (SPONTANEOUS_ORDER) {parameter}
NODE_CYCLE: 125                     # delta process round period {from 6. Evaluation introduction}
NODE_DRIFT: 0.01                    # round drift (1% = 1/100 = 0.01) {from 6. Evaluation introduction}
LATENCY_TABLE: data/latencies.obj   # latencies from planetlab {from 6. Evaluation introduction}
LATENCY_DRIFT: None                 # Not used (not implemented) {according to source code}
fanout: 17                          # Fanout (K=?) {2*e*ln(100) / (ln(ln(100))) -> Computed as 16.3938... according to K}
TTL: 20                             # Time to live, {Global: TTL >= (c+1)*log2(n)*(delta_max / delta_min)}
                                    # {Logical: TTL >= 2 * (c+1) * log2(n) * (delta_max / delta_min)}
MEASURE_ORDER: False                # @FIXME Don't know what is it
CHURN : False                       # No churn 
CHURN_RATE : 0.05                   # No churn 
MESSASE_LOSS: 0.0                   # No message loss
EOF
pypy ./epto-vanilla.py ./conf_out 3

# Global clock with EpTO
sed -i 's/IS_CLOCK_GLOBAL: False/IS_CLOCK_GLOBAL: True/' ./conf_out/conf.yaml
sed -i 's/TTL: [0-9]*/TTL: 11/' ./conf_out/conf.yaml
pypy ./epto-vanilla.py ./conf_out 2

# Regular gossip without ordering
sed -i 's/ORDER_MECHANISM: STABLE_ORDER/ORDER_MECHANISM: SPONTANEOUS_ORDER/' ./conf_out/conf.yaml
pypy ./epto-vanilla.py ./conf_out 1
fi

gnuplot --persist <<EOF
set key outside
set key right top
set terminal svg enhanced background rgb 'white'
set terminal svg size 850,220
set output './conf_out/deliveryDelay.svg'
set xrange [0:4000]

plot './conf_out/deliveryDelay-1.gpData' using 2:1 with lines title "baseline (no order)", \
     './conf_out/deliveryDelay-2.gpData' using 2:1 with lines title "global clock", \
     './conf_out/deliveryDelay-3.gpData' using 2:1 with line title "logical clock"
EOF

xdg-open ./conf_out/deliveryDelay.svg

