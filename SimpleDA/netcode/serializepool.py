import pool, coding

class SerializePool(pool.Pool):
    def __init__(self, deliver, send, fanout = 8, maxRank = 20, split = 0, hist = 10):
        deliver2 = lambda x, y, ttl: self.__deliver_wrapper(x, y, deliver, ttl)
        pool.Pool.__init__(self, deliver2, send, fanout=fanout, maxRank=maxRank, split=split, hist=0)

    def broadcast(self, msg):
        #print "broadcast", msg
        pool.Pool.broadcast(self, coding.serialize(msg))
    
    def __deliver_wrapper(self, uuid, msg, f, ttl):
        decoded = coding.deserialize(msg)
        #print "deliver decoded", decoded
        f(uuid, decoded, ttl)
