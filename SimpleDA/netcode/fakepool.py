import pool

"""
  This class is specific to EpTO
"""
class FakePool(pool.Pool):
    def __init__(self, deliver, send, fanout = 8, maxRank = 20, split=0):
        pool.Pool.__init__(self, deliver, send, fanout=fanout, maxRank=maxRank, split=split)
        self.fanout = fanout
        self.delivered = set()

    def receive(self, p):
        gid,c,e,ttl = p
        if e not in self.delivered:
            self.delivered.add(e)

            p = (gid,c,e,ttl+1)
            self.send_cb([p] * self.fanout)
            self.deliver_cb(0, e, ttl+1)

    def broadcast(self, msg):
        p = (0, [(1, 1)], msg, 0)
        self.send_cb([p] * self.fanout)
        self.delivered.add(msg)

