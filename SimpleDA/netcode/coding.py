import json, pyflexcode

def serialize(m):
    enc = json.dumps(m)
    ret = pyflexcode.ffi.new("unsigned char[]", enc+"\x00")
    return ret

def deserialize(m):
    """
    limit = len(m)
    while m[limit-1] == 0:
        limit -= 1
    json_string = ''.join([chr(x) for x in m[0:limit]])
    return json.loads(json_string)
    """
    return json.loads(m) 
