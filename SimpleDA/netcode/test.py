import unittest, generation, pool, packet, serializepool

class TestGeneration(unittest.TestCase):
    def testAddAndDeliverSimple(self):
        g = generation.Generation()
        g.add({"aBzc32": 1}, [42, 43, 44])
        
        result = g.deliver()
        result[0] = (result[0][0], [z for z in result[0][1]])
        self.assertEqual(result, [("aBzc32", [42, 43, 44])])

        result = g.deliver()
        self.assertEqual(result, [])
        g.close()

    def testRecode(self):
        exp_res = [
            ("aBzc32", [42,43,44]),
            ("EfnT7i", [100,110,120]),
            ("lOptBc", [1, 11, 111])
        ]

        node1_g = generation.Generation()
        node1_g.add({"aBzc32": 1}, [42, 43, 44])
        node1_g.add({"EfnT7i": 1}, [100, 110, 120])
        node1_g.add({"lOptBc": 1}, [1, 11, 111])

        res = [ (x[0], [z for z in x[1]]) for x in sorted(node1_g.deliver())]
        self.assertEqual(res, sorted(exp_res))

        node2_g = generation.Generation()
        for i in range(3):
            c, e = node1_g.recode()
            node2_g.add(c, e)

        res = [ (x[0], [z for z in x[1]]) for x in sorted(node2_g.deliver())]
        self.assertEqual(res, sorted(exp_res))

        self.assertEqual(node1_g.deliver(), [])
        self.assertEqual(node2_g.deliver(), [])
        
        node1_g.close()
        node2_g.close()

    def testAddSameInfo(self):
        node1_g = generation.Generation()
        self.assertTrue(node1_g.add({"A": 2, "B": 2, "C": 2}, [6, 6, 6]))
        self.assertFalse(node1_g.add({"A": 1, "B": 1, "C": 1}, [3, 3, 3]))
        self.assertFalse(node1_g.add({"A": 4, "B": 4, "C": 4}, [12, 12, 12]))
        self.assertEqual(node1_g.deliver(), [])
        self.assertEqual(node1_g.getRank(), 1)

        node1_g.close()

    def testDifferentSize(self):
        exp_res = [
            ("aBzc32", [42,43,44,44,44,44]),
            ("EfnT7i", [100,110,120,120,120,120]),
            ("lOptBc", [1, 11, 111, 2, 22, 222])
        ]

        node1_g = generation.Generation()
        node1_g.add({"aBzc32": 1}, [42, 43, 44])
        node1_g.add({"EfnT7i": 1}, [100, 110, 120])
        node1_g.add({"lOptBc": 1}, [1, 11, 111, 2, 22, 222])
        res = [ (x[0], [z for z in x[1]]) for x in sorted(node1_g.deliver())]
        self.assertEqual(res, sorted(exp_res))

        node2_g = generation.Generation()
        for i in range(3):
            c, e = node1_g.recode()
            node2_g.add(c, e)

        res = [ (x[0], [z for z in x[1]]) for x in sorted(node2_g.deliver())]
        self.assertEqual(sorted(res), sorted(exp_res))

        self.assertEqual(node1_g.deliver(), [])
        self.assertEqual(node2_g.deliver(), [])

        node1_g.close()
        node2_g.close()

class TestPool(unittest.TestCase):
    def testReceive(self):
        toSend = []
        toDeliver = []
        send = lambda x: toSend.extend(x)
        deliver = (lambda x, y: toDeliver.append((x, [z for z in y])))
       
        res1 = [('A', [43, 42, 41, 40])]
        res2 = res1 + [('B', [1, 1, 1, 1])]
        res3 = res2 + [('C', [8, 8, 8, 8])]
        res3 = res3 + [('D', [2, 2, 2, 2])]

        po = pool.Pool(deliver, send, fanout=1)
        self.assertEqual(po.currentGeneration, 0)
        pckt = packet.Packet(5, {"A": 1}, [43, 42, 41, 40])

        po.receive(pckt)
        self.assertEqual(toDeliver, res1)
        self.assertEqual(len(toSend), 1)
        self.assertEqual(po.currentGeneration, 5)

        po.receive(packet.Packet(5, {"A": 1, "B": 1}, [42, 43, 40, 41]))
        self.assertEqual(toDeliver, res2)
        self.assertEqual(len(toSend), 2)

        po.receive(packet.Packet(5, {"C": 1, "D": 1}, [10, 10, 10, 10]))
        self.assertEqual(toDeliver, res2)
        self.assertEqual(len(toSend), 3)
        
        po.receive(packet.Packet(5, {"C": 1, "D": 1}, [10, 10, 10, 10]))
        self.assertEqual(toDeliver, res2)
        self.assertEqual(len(toSend), 3)
        
        po.receive(packet.Packet(5, {"C": 3, "D": 5}, [18, 18, 18, 18]))
        self.assertEqual(toDeliver, res3)
        self.assertEqual(len(toSend), 4)

    def testBroadcast(self):
        toSend = []
        toDeliver = []
        send = lambda x: toSend.extend(x)
        deliver = (lambda x, y: toDeliver.append((x, [z for z in y])))
        
        po = pool.Pool(deliver, send, fanout=2, maxRank=2)
        self.assertEqual(po.currentGeneration, 0)

        po.broadcast([43,42,41,40])
        self.assertEqual(toDeliver, [])
        self.assertEqual(len(toSend), 2)
        self.assertEqual(po.currentGeneration, 0)

        po.broadcast([12,12,12,12])
        self.assertEqual(toDeliver, [])
        self.assertEqual(len(toSend), 4)
        self.assertEqual(po.currentGeneration, 0)
        
        po.broadcast([12,12,12,13])
        self.assertEqual(toDeliver, [])
        self.assertEqual(len(toSend), 6)
        self.assertEqual(po.currentGeneration, 1)
        
        po.broadcast([12,12,12,14])
        self.assertEqual(toDeliver, [])
        self.assertEqual(len(toSend), 8)
        self.assertEqual(po.currentGeneration, 1)
        self.assertEqual(len(filter(lambda x: x.generation == 0, toSend)), 6)
        self.assertEqual(len(filter(lambda x: x.generation == 1, toSend)), 2)

if __name__ == '__main__':
    unittest.main()
